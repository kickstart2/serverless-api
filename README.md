# Serverless API for Recipes app 

![Badge statements](./badges/badge-statements.svg)
![Badge function](./badges/badge-functions.svg)
![Badge branches](./badges/badge-branches.svg)
![Badge lines](./badges/badge-lines.svg)

---
Node.js solution for AWS Cloud​Formation, Lambda and DynamoDB built on [Serverless framework](https://www.serverless.com/framework/docs/providers/aws/guide/installation/)  

### Requirements

- [Install the Serverless Framework](https://serverless.com/framework/docs/providers/aws/guide/installation/)
- [Configure your AWS CLI](https://serverless.com/framework/docs/providers/aws/guide/credentials/)

### Installation

Install the Node.js packages

``` bash
$ npm install
```

### Usage

To run a function on local

``` bash
$ serverless invoke local --function hello
```

To simulate API Gateway locally using [serverless-offline](https://github.com/dherault/serverless-offline)

``` bash
$ serverless offline start
```

Deploy project

``` bash
$ serverless deploy
```

Deploy a single function

``` bash
$ serverless deploy function --function hello
```

#### Running Tests

Run tests using

``` bash
$ npm test
```


---
