import checker from "../libs/envVarChecker";

describe(`Utility library envVarsChecker`, () => {
    test(`The helper exists`, () => {
        expect(checker).toBeTruthy();
    });

    test(`Asks for both tableName and region environment variables`, () => {
        const input = {};
        const result = checker(input);
        expect(result).toEqual(['tableName', 'region']);
    });

    test(`Asks for a missing tableName environment variables`, () => {
        const input = {
            region: 'foo',
        };
        const result = checker(input);
        expect(result).toEqual(['tableName']);
    });

    test(`Asks for a missing region environment variables`, () => {
        const input = {
            tableName: 'foo',
        };
        const result = checker(input);
        expect(result).toEqual(['region']);
    });
});
