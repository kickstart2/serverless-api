import dynamodb from "../libs/dynamodb-lib";

describe("DynamoDB Library", () => {
    test("should support get, put, query, update, delete", () => {
        expect(dynamodb.get).toBeTruthy();
        expect(dynamodb.put).toBeTruthy();
        expect(dynamodb.query).toBeTruthy();
        expect(dynamodb.update).toBeTruthy();
        expect(dynamodb.delete).toBeTruthy();
    });
});


