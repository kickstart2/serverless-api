import AWSMock from "aws-sdk-mock";
import * as AWS from "aws-sdk";
import * as deleteAction from "../api/delete";
import deleteEventStub from "../mocks/delete-event";
const deleteResponseStub = {};
const awsMethod = "delete";
const restoreMock = () => AWSMock.restore("DynamoDB.DocumentClient", awsMethod);

describe("API delete", () => {
    test("delete is available", () => {
        expect(deleteAction.main).toBeTruthy();
    });

    test("request body has necessary properties", (done) => {
        process.env.tableName = "foo";
        process.env.region = "bar";
        const checkRequestBody = (body) => {
            try
            {
                expect(body.TableName).toBe(process.env.tableName);
                expect(body.Key).toBeTruthy();
                expect(body.Key["userId"]).toBe(deleteEventStub.requestContext.identity.cognitoIdentityId);
                expect(body.Key["recipeId"]).toBe(deleteEventStub.pathParameters.id);
                done();
            } catch (e)
            {
                done(e);
            }

        };

        AWSMock.setSDKInstance(AWS);
        AWSMock.mock("DynamoDB.DocumentClient", awsMethod, (requestBody, callback) => {
            checkRequestBody(requestBody);
            callback(null, deleteResponseStub);
        });

        const event = deleteEventStub;
        const context = {};
        const result = deleteAction.main(event, context);
        result.then((response) => {
            restoreMock();
        });
    });

    test("replies back with a empty object on success", (done) => {
        process.env.tableName = "foo";
        process.env.region = "bar";

        AWSMock.setSDKInstance(AWS);
        AWSMock.mock("DynamoDB.DocumentClient", awsMethod, (requestBody, callback) => {
            callback(null, deleteResponseStub);
        });


        const event = deleteEventStub;
        const context = {};
        const result = deleteAction.main(event, context);
        return result
            .then((response) => {
                expect(typeof response.body).toBe("string");
                expect(JSON.parse(response.body)).toStrictEqual(deleteResponseStub);
                restoreMock();
                done();
            })
            .catch(e => {
                restoreMock();
                done(e);
            })
    });

    test("replies back with a Error on network failure", (done) => {
        process.env.tableName = "foo";
        process.env.region = "bar";

        AWSMock.setSDKInstance(AWS);
        AWSMock.mock("DynamoDB.DocumentClient", awsMethod, (requestBody, callback) => {
            // console.log("REQUESST:\n", requestBody);
            callback({message:"network blah"}, null);
        });

        const event = deleteEventStub;
        const context = {};
        const result = deleteAction.main(event, context);
        return result
            .then((response) => {
                expect(typeof response.body).toBe("string");
                expect(JSON.parse(response.body).error).toBe("network blah");
                restoreMock();
                done();
            })
            .catch(e => {
                restoreMock();
                done(e);
            })


    });
});
