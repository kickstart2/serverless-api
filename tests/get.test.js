import AWSMock from "aws-sdk-mock";
import * as AWS from "aws-sdk";
import * as get from "../api/get";
import getEventStub from "../mocks/get-event";
const getResponseStub = {Item: {userId:"USER-SUB-1234",recipeId:"08e9d110-bd3c-11ea-b658-7be76efde3e7", content:"hello world", attachment:"hello.jpg",createdAt:1593787613345}};
const awsMethod = "get";
const restoreMock = () => AWSMock.restore("DynamoDB.DocumentClient", awsMethod);
describe("API get", () => {

    test("get is available", () => {
        expect(get.main).toBeTruthy();
    });

    test("replies back with a JSON on success", (done) => {
        process.env.tableName = "foo";
        process.env.region = "bar";

        AWSMock.setSDKInstance(AWS);
        AWSMock.mock("DynamoDB.DocumentClient", awsMethod, (requestBody, callback) => {
            // console.log("REQUEST:\n", requestBody);
            callback(null, getResponseStub);
        });


        const event = getEventStub;
        const context = {};
        const result = get.main(event, context);
        return result
            .then((response) => {
                // console.log("response", response);
                expect(typeof response.body).toBe("string");
                expect(JSON.parse(response.body)).toStrictEqual(getResponseStub.Item);//toMatchSnapshot()
                restoreMock();
                done();
            })
            .catch(e => {
                restoreMock();
                done(e);
            })
    });

    test("replies back with a Error on failure", (done) => {
        process.env.tableName = "foo";
        process.env.region = "bar";

        AWSMock.setSDKInstance(AWS);
        AWSMock.mock("DynamoDB.DocumentClient", awsMethod, (requestBody, callback) => {
            // console.log("REQUEST:\n", requestBody);
            callback(null, {
                data: "no data"
            });
        });


        const event = getEventStub;
        const context = {};
        const result = get.main(event, context);
        return result
            .then((response) => {
                expect(typeof response.body).toBe("string");
                expect(JSON.parse(response.body).error).toBe("Item not found.");
                restoreMock();
                done();
            })
            .catch(e => {
                restoreMock();
                done(e);
            })


    });

    test("replies back with a Error on network failure", (done) => {
        process.env.tableName = "foo";
        process.env.region = "bar";

        AWSMock.setSDKInstance(AWS);
        AWSMock.mock("DynamoDB.DocumentClient", awsMethod, (requestBody, callback) => {
            // console.log("REQUEST:\n", requestBody);
            callback({message:"network blah"}, null);
        });

        const event = getEventStub;
        const context = {};
        const result = get.main(event, context);
        return result
            .then((response) => {
                expect(typeof response.body).toBe("string");
                expect(JSON.parse(response.body).error).toBe("network blah");
                restoreMock();
                done();
            })
            .catch(e => {
                restoreMock();
                done(e);
            });
    });
});
