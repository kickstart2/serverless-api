import AWSMock from "aws-sdk-mock";
import * as AWS from "aws-sdk";
import * as list from "../api/list";
import listEventStub from "../mocks/get-event";
const getResponseStub = {Items: [{userId:"USER-SUB-1234",recipeId:"08e9d110-bd3c-11ea-b658-7be76efde3e7", content:"hello world", attachment:"hello.jpg",createdAt:1593787613345}]};
const awsMethod = "query";
const restoreMock = () => AWSMock.restore("DynamoDB.DocumentClient", awsMethod);
describe("API list", () => {

    test("list is available", () => {
        expect(list.main).toBeTruthy();
    });

    test("request body has necessary properties", (done) => {
        process.env.tableName = "foo";
        process.env.region = "bar";
        const checkRequestBody = (body) => {
            try {
                expect(body.TableName).toBe(process.env.tableName);
                expect(body.KeyConditionExpression).toBe("userId = :userId");
                expect(body.ExpressionAttributeValues).toBeTruthy();
                expect(body.ExpressionAttributeValues[":userId"]).toBe(listEventStub.requestContext.identity.cognitoIdentityId);
            } catch (e) { done(e); }
            done();
        };

        AWSMock.setSDKInstance(AWS);
        AWSMock.mock("DynamoDB.DocumentClient", awsMethod, (requestBody, callback) => {
            checkRequestBody(requestBody);
            callback(null, getResponseStub);
        });

        const event = listEventStub;
        const context = {};
        const result = list.main(event, context);
        result
            .then((response) => {
                restoreMock();
            })
            .catch(e => {
                restoreMock();
            });
    });

    test("replies back with a JSON on success", (done) => {
        process.env.tableName = "foo";
        process.env.region = "bar";

        AWSMock.setSDKInstance(AWS);
        AWSMock.mock("DynamoDB.DocumentClient", awsMethod, (requestBody, callback) => {
            callback(null, getResponseStub);
        });


        const event = listEventStub;
        const context = {};
        const result = list.main(event, context);
        return result
            .then((response) => {
                restoreMock();
                expect(typeof response.body).toBe("string");
                expect(JSON.parse(response.body)).toStrictEqual(getResponseStub.Items);//toMatchSnapshot()
                done();
            })
            .catch(e => {
                restoreMock();
                done(e);
            })
    });

    test("replies back with a Error on network failure", (done) => {
        process.env.tableName = "foo";
        process.env.region = "bar";

        AWSMock.setSDKInstance(AWS);
        AWSMock.mock("DynamoDB.DocumentClient", awsMethod, (requestBody, callback) => {
            // console.log("REQUEST:\n", requestBody);
            callback({message:"network blah"}, null);
        });

        const event = listEventStub;
        const context = {};
        const result = list.main(event, context);
        return result
            .then((response) => {
                expect(typeof response.body).toBe("string");
                expect(JSON.parse(response.body).error).toBe("network blah");
                restoreMock();
                done();
            })
            .catch(e => {
                restoreMock();
                done(e);
            });
    });

    test("replies back with a Error on response body failure", (done) => {
        process.env.tableName = "foo";
        process.env.region = "bar";

        AWSMock.setSDKInstance(AWS);
        AWSMock.mock("DynamoDB.DocumentClient", awsMethod, (requestBody, callback) => {
            // console.log("REQUEST:\n", requestBody);
            callback(null, {
                data: "no data"
            });
        });

        const event = listEventStub;
        const context = {};
        const result = list.main(event, context);
        return result
            .then((response) => {
                expect(typeof response.body).toBe("string");
                expect(JSON.parse(response.body).error).toBe("Items not found.");
                restoreMock();
                done();
            })
            .catch(e => done(e));
    })
});
