import handler from "../libs/handler-lib";

const event = "event";
const context = "context";
const bodySuccess = {userId:"USER-SUB-1234",recipeId:"08e9d110-bd3c-11ea-b658-7be76efde3e7", content:"hello world", attachment:"hello.jpg",createdAt:1593787613345};
const bodyFailure = {message:"lambda failed"};
const headers = {
    "Access-Control-Allow-Origin": "*",
    "Access-Control-Allow-Credentials": true
};

const lambdaOk = (event, context) => Promise.resolve(bodySuccess).catch(e => bodySuccess);
const lambdaNotOk = (event, context) => Promise.reject(bodyFailure);

describe("Handler", () => {
    beforeAll(() => {
        process.env.tableName = 'foo';
        process.env.region = 'bar';
    });

    test("handler exists", () => {
        return expect(handler).toBeTruthy();
    });

    test("mock lambdaOk resolves to bodySuccess", () => {
        return expect(lambdaOk()).resolves.toBe(bodySuccess);
    });

    test("mock lambdaNotOk rejects to bodyFailure", () => {
        return expect(lambdaNotOk()).rejects.toBe(bodyFailure);
    });

    test("should handle lambdaOk with status 200", done => {
        const callback = (response) => {
            expect(response.statusCode).toEqual(200);
            expect(typeof response.body).toEqual("string");
            done();
        };
        handler(lambdaOk)(event, context)
            .then((callback));
    });

    test("should handle lambdaNotOk with status 500", done => {
        const callback = (response) => {
            expect(response.statusCode).toEqual(500);
            expect(typeof response.body).toEqual("string");
            expect(JSON.parse(response.body).error).toEqual(bodyFailure.message);
            done();
        };
        handler(lambdaNotOk)(event, context)
            .then((callback));
    });

    test("response should contain CORS headers", done => {
        const callback = (response) => {
            expect(response.headers).toEqual(headers);
            done();
        };
        handler(lambdaOk)(event, context)
            .then((callback));
    });

    test("require environment variables", () => {
        delete process.env.tableName;
        delete process.env.region;
        const event = {};
        const context = {};
        const result = handler(lambdaOk)(event, context);
        return result
            .then(response => expect(JSON.parse(response.body).error).toBe("Missing required environment variables: tableName, region"));
    });
});


