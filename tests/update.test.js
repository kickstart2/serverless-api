import AWSMock from "aws-sdk-mock";
import * as AWS from "aws-sdk";
import * as update from "../api/update";
import updateEventStub from "../mocks/update-event";
const updateResponseStub = {"Attributes":{"attachment":"new.jpg","content":"new world","createdAt":1593952572450,"recipeId":"1c33b020-bebc-11ea-ab77-bdca233e651d","userId":"USER-SUB-1234"}};
const awsMethod = "update";
const restoreMock = () => AWSMock.restore("DynamoDB.DocumentClient", awsMethod);

describe("API update", () => {
    test("update is available", () => {
        expect(update.main).toBeTruthy();
    });

    test("request body has necessary properties", (done) => {
        process.env.tableName = "foo";
        process.env.region = "bar";
        const checkRequestBody = (body) => {
            try
            {
                expect(body.TableName).toBe(process.env.tableName);
                expect(body.UpdateExpression).toBe("SET content = :content, attachment = :attachment");
                expect(body.ExpressionAttributeValues).toBeTruthy();
                expect(body.ExpressionAttributeValues[":attachment"]).toBe(JSON.parse(updateEventStub.body).attachment);
                expect(body.ExpressionAttributeValues[":content"]).toBe(JSON.parse(updateEventStub.body).content);
                expect(body.Key).toBeTruthy();
                expect(body.Key["userId"]).toBe(updateEventStub.requestContext.identity.cognitoIdentityId);
                expect(body.Key["recipeId"]).toBe(updateEventStub.pathParameters.id);
                expect(body.ReturnValues).toBe("ALL_NEW");
                done();
            } catch (e)
            {
                done(e);
            }

        };

        AWSMock.setSDKInstance(AWS);
        AWSMock.mock("DynamoDB.DocumentClient", awsMethod, (requestBody, callback) => {
            checkRequestBody(requestBody);
            callback(null, updateResponseStub);
        });

        const event = updateEventStub;
        const context = {};
        const result = update.main(event, context);
        result.then((response) => {
            restoreMock();
        });
    });

    test("replies back with a JSON on success", (done) => {
        process.env.tableName = "foo";
        process.env.region = "bar";

        AWSMock.setSDKInstance(AWS);
        AWSMock.mock("DynamoDB.DocumentClient", awsMethod, (requestBody, callback) => {
            callback(null, updateResponseStub);
        });


        const event = updateEventStub;
        const context = {};
        const result = update.main(event, context);
        return result
            .then((response) => {
                expect(typeof response.body).toBe("string");
                expect(JSON.parse(response.body)).toStrictEqual(updateResponseStub.Attributes);//toMatchSnapshot()
                restoreMock();
                done();
            })
            .catch(e => {
                restoreMock();
                done(e);
            })
    });

    test("replies back with a Error on network failure", (done) => {
        process.env.tableName = "foo";
        process.env.region = "bar";

        AWSMock.setSDKInstance(AWS);
        AWSMock.mock("DynamoDB.DocumentClient", awsMethod, (requestBody, callback) => {
            // console.log("REQUESST:\n", requestBody);
            callback({message:"network blah"}, null);
        });

        const event = updateEventStub;
        const context = {};
        const result = update.main(event, context);
        return result
            .then((response) => {
                expect(typeof response.body).toBe("string");
                expect(JSON.parse(response.body).error).toBe("network blah");
                restoreMock();
                done();
            })
            .catch(e => {
                restoreMock();
                done(e);
            })


    });

    test("replies back with a Error on response body failure", (done) => {
        process.env.tableName = "foo";
        process.env.region = "bar";

        AWSMock.setSDKInstance(AWS);
        AWSMock.mock("DynamoDB.DocumentClient", awsMethod, (requestBody, callback) => {
            // console.log("REQUEST:\n", requestBody);
            callback(null, {
                status: false
            });
        });

        const event = updateEventStub;
        const context = {};
        const result = update.main(event, context);
        return result
            .then((response) => {
                expect(typeof response.body).toBe("string");
                expect(JSON.parse(response.body).error).toBe("Update failed.");
                restoreMock();
                done();
            })
            .catch(e => done(e));


    })
});
