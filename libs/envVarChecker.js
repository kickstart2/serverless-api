export default env => {
    // const required = ['BUCKET', 'REGION'];
    const required = ['tableName', 'region'];
    const missing = [];

    required.forEach(reqVar => {
        if (!env[reqVar]) {
            missing.push(reqVar);
        }
    });

    return missing;
};
