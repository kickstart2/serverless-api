import checker from "./envVarChecker";

const checkEnvironment = () => new Promise((resolve) => {
    const result = checker(process.env);
    if (result.length === 0) {
        return resolve();
    }
    throw new Error("Missing required environment variables: " + result.join(", "));
});

export default function handler(lambda) {
    return (event, context) => {
        return Promise.resolve()
            .then(() => checkEnvironment())
            .then(() => lambda(event, context))
            .then((responseBody) => [200, responseBody])
            .catch((e) => [500, {error:e.message}])
            .then(([statusCode, body]) => ({
                statusCode,
                body: JSON.stringify(body),
                headers: {
                    "Access-Control-Allow-Origin": "*",
                    "Access-Control-Allow-Credentials": true
                }
            }));

    };
};
