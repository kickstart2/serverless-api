import handler from "../libs/handler-lib";
import dynamodb from "../libs/dynamodb-lib";

export const main = handler((event, context) => new Promise((resolve, reject) => {
    const params = {
        TableName: process.env.tableName,
        // 'Key' defines the partition key and sort key of the item to be updated
        // - 'userId': Identity Pool identity id of the authenticated user
        // - 'recipeId': path parameter
        Key: {
            userId: event.requestContext.identity.cognitoIdentityId,
            recipeId: event.pathParameters.id
        }
    };
    dynamodb.delete(params)
        .then((response) => {
            resolve(response);
        })
        .catch((e) => reject(e));
}));
