import handler from "../libs/handler-lib";
import dynamodb from "../libs/dynamodb-lib";

export const main = handler((event, context) => new Promise((resolve, reject) => {
    const params = {
        TableName: process.env.tableName,
        // 'KeyConditionExpression' defines the condition for the query
        // - 'userId = :userId': only return items with matching 'userId'
        //   partition key
        // 'ExpressionAttributeValues' defines the value in the condition
        // - ':userId': defines 'userId' to be Identity Pool identity id
        //   of the authenticated user
        KeyConditionExpression: "userId = :userId",
        ExpressionAttributeValues: {
            ":userId": event.requestContext.identity.cognitoIdentityId
        }

    };
    dynamodb.query(params)
        .then((result) => {
            if (!result.Items) {
                throw new Error("Items not found.");
            }
            resolve(result.Items);
        })
        .catch((e) => {
            // console.log("LIST", e);
            reject(e);
        });
}));
